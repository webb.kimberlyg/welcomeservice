package kw.welcomeServive;

import java.util.Scanner;

import kw.welcomelib.IWelcomeService;

public class Driver {

	public static void main(String[] args) {
		Scanner input= new Scanner(System.in);
		IWelcomeService service = new WelcomeService();
		System.out.println("please enter your name");
		String name = input.nextLine();
		System.out.println(service.getWelcomeMessage(name));
		input.close();

	}

}
